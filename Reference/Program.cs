﻿using System;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
using System.Threading;

using CASO.AX.Net;

namespace DDNJ_AX_Import
{
    class Program
    {
        static string _programName = ConfigurationManager.AppSettings["ProgramName"].ToString();
        static string _programExeName = ConfigurationManager.AppSettings["ProgramExeName"].ToString();
        static string _connString = ConfigurationManager.AppSettings["ConnectionString"].ToString();
        static string _aXAppName = ConfigurationManager.AppSettings["AXAppName"].ToString();
        static string[] _inputDirectories = ConfigurationManager.AppSettings["InputDirectories"].Split(';');
        static string _sendErrorEmails = ConfigurationManager.AppSettings["SendErrorEmails"];
        static string _sendStatusEmails = ConfigurationManager.AppSettings["SendStatusEmails"];
        static string[] _recordLifetimes = ConfigurationManager.AppSettings["RecordLifetimes"].Split(';');
        static string[] _inputFrequencies = ConfigurationManager.AppSettings["InputFrequencies"].Split(';');

        static void Main(string[] args)
        {
            try
            {
                // only 1 instance can be running at a time !!!
                System.Diagnostics.Process[] runningInstances = System.Diagnostics.Process.GetProcessesByName(_programExeName);
                if (runningInstances.Length > 1)
                {
                    throw new ApplicationException(_programExeName + " already running, aborting this instance.");
                }

                // should we add log entry that program ran
                string logHeartBeatMessage = ConfigurationManager.AppSettings["DebugHeartBeat Message"].ToString().ToUpper(); ;
                try
                {
                    if (logHeartBeatMessage == "TRUE")
                        LogMsg(_programName + " - started");
                }
                catch (Exception exB)
                {
                    LogMsg(_programName + " - Error encountered. Error: " + exB);
                    if (_sendErrorEmails == "true")
                        SendErrorEmail(_programExeName + " - Error encountered", "Error: " + exB);
                    // should program terminate or keep going?
                    // throw new ApplicationException(_programExeName + " - Failed to log heartbeat entry. Error: " + exB.Message);
                }

                int processedCount = 0;
                DateTime startTime = DateTime.Now;

                SetupRuntimeLog();

                //loop through directories
                for (int d = 0; d < _inputDirectories.Length; d++) //foreach ( string dir in _inputDirectories)
                {
                    //check to see if time to process
                    string sLastRuntime = GetLastRuntime(d);
                    sLastRuntime = sLastRuntime.Substring(0, sLastRuntime.LastIndexOf("|"));
                    if (sLastRuntime != "")
                    {
                        DateTime lastRuntime = DateTime.Parse(sLastRuntime);
                        TimeSpan timeSince = DateTime.Now - lastRuntime;
                        int frequency = int.Parse(_inputFrequencies[d]);
                        //if it's not time, skip this directory
                        if (timeSince.Minutes < frequency)
                            continue;
                    }
                    //otherwise record the runtime
                    RecordRuntime(d);

                    string dir = _inputDirectories[d];
                    int recordLifetime = -1;
                    int.TryParse(_recordLifetimes[d], out recordLifetime);
                    //Delete previously processed records from the Processed folder
                    DeleteOldRecords(dir, recordLifetime);

                    //get list of filepaths
                    var files = Directory.EnumerateFiles(dir);
                    //loop through files in dir
                    foreach (string file in files)
                    {
                        //Create and Index document in AX
                        byte[] imgBytes = System.IO.File.ReadAllBytes(file);
                        string recordName = file.Substring(file.LastIndexOf(@"\") + 1);
                        string[] fields = { "RECORD", "TIMESTAMP", "STATUS" };
                        string[] values = { recordName, System.DateTime.Now.ToString(), "NEW" };
                        ApplicationXtender AX = new ApplicationXtender(_connString);
                        AXApplication APP = AX.Applications[_aXAppName];
                        AXIndexSet indexSet = new AXIndexSet(APP);
                        for (int i = 0; i < fields.Length; i += 1)
                        {
                            indexSet[fields[i]].Value = values[i];
                        }

                        //AXDocument doc = APP.CreateDocument(imgBytes, ".pdf", false, indexSet);                        
                        AXDocument doc = APP.CreateDocument(file, indexSet, false);

                        //attempt to check correctness
                        //TODO

                        //Move processed file to Processed folder
                        string newFile = file.Substring(0, file.LastIndexOf(@"\") + 1) + @"Processed\" + recordName;
                        Console.WriteLine("Moving processed file to Processed folder at " + newFile);
                        int numTry = 0;
                        while (System.IO.File.Exists(newFile))
                        {
                            numTry++;
                            string suffix = newFile.Substring(newFile.LastIndexOf('.'));
                            newFile = newFile.Substring(0, newFile.LastIndexOf(suffix)) + "_" + numTry + suffix;
                        }

                        System.IO.File.SetCreationTime(file, DateTime.Now);
                        System.IO.File.SetLastWriteTime(file, DateTime.Now);
                        System.IO.File.Move(file, newFile);

                        //Log the action
                        processedCount++;
                        LogMsg(file + " processed");
                    }
                }

                Console.WriteLine("End - " + (DateTime.Now - startTime).TotalMilliseconds + "ms elasped");
                //Console.WriteLine("Press any key to exit...");
                //Console.ReadKey();

                // send email
                if (_sendStatusEmails == "true")
                    EmailNotification_RunComplete("DDNJ AX Import Complete", processedCount.ToString() + " records imported to AX");

                // should we add log entry that program completed (successfully)
                try
                {
                    if (logHeartBeatMessage == "TRUE")
                        LogMsg(_programName + " - completed");
                }
                catch (Exception exB)
                {
                    LogMsg(_programName + " - Error encountered. Error: " + exB);
                    if (_sendErrorEmails == "true")
                        SendErrorEmail(_programExeName + " - Error encountered", "Error: " + exB);
                    // should program terminate or keep going?
                    // throw new ApplicationException(_programExeName + " - Failed to log heartbeat entry. Error: " + exB.Message);
                }

            }
            catch (Exception ex)
            {
                //LogMsg(_programName + " - Error encountered. Error: " + ex.Message);
                if (_sendErrorEmails == "true")
                    SendErrorEmail(_programExeName + " - Error encountered", "Error: " + ex);  // make sure not to send duplicate error emails
            }
        }

        private static void SetupRuntimeLog()
        {
            for ( int d = 0; d < _inputDirectories.Length; d++)
            {
                string path = d.ToString() + "_runtime.txt";
                if (!File.Exists(path))
                    File.Create(path);
            }
        }

        static private void DeleteOldRecords(string directory, int recordLifetime)
        {
            //get list of filepaths in the Processed folder
            var files = Directory.EnumerateFiles(directory + @"\Processed");
            //loop through files in dir
            foreach (string file in files)
            {
                //Check date
                DateTime createDate = File.GetCreationTime(file);
                int dateDiff = (DateTime.Now - createDate).Days;
                int timeDiff = (DateTime.Now - createDate).Minutes;
                //delete if Processed record is older than max lifetime
                if (timeDiff > recordLifetime)
                {
                    Console.WriteLine("Deleting old record at " + file);
                    File.Delete(file);
                }
            }
        }

        #region "Runtime log"
        
        static string _runtimeLogMutexName = ConfigurationManager.AppSettings["RuntimeLogMutex"].ToString();
        static Mutex _runtimeLogMutex = new Mutex(false, _runtimeLogMutexName);
        static private void RecordRuntime(int directoryNumber)
        {
            try
            {
                _runtimeLogMutex.WaitOne();  // mutex prevents multiple instances of program from interleaving text
                //clear previous runtime
                File.WriteAllText(directoryNumber + "_runtime.txt", "");
                using (System.IO.StreamWriter log = new System.IO.StreamWriter(directoryNumber + "_runtime.txt", true))
                {
                    log.WriteLine(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss | "));
                    log.Close();
                }
                _runtimeLogMutex.ReleaseMutex();
            }
            catch (Exception ex)
            {
                throw new ApplicationException("RecordRuntime() : " + ex.Message);
            }
        }
        static private string GetLastRuntime(int directoryNumber)
        {
            try
            {
                string lastRuntime = File.ReadAllText(directoryNumber + "_runtime.txt");
                return lastRuntime;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("RecordRuntime() : " + ex.Message);
            }
        }

        #endregion

        #region "Support routines"

        static string _debug = ConfigurationManager.AppSettings["Debug"].ToString().ToUpper();
        static string _debugFileName = ConfigurationManager.AppSettings["DebugFilename"].ToString();
        static string _debugMutexName = ConfigurationManager.AppSettings["DebugMutex"].ToString();
        static Mutex _debugMutex = new Mutex(false, _debugMutexName);
        static private void LogMsg(string message)
        {
            try
            {
                if (_debug == "TRUE")
                {
                    _debugMutex.WaitOne();  // mutex prevents multiple instances of program from interleaving text
                    using (System.IO.StreamWriter log = new System.IO.StreamWriter(_debugFileName, true))
                    {
                        log.WriteLine(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss | ") + message);
                        log.Close();
                    }
                    _debugMutex.ReleaseMutex();
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("LogMsg() : " + ex.Message);
            }
        }


        static private void EmailNotification_RunComplete(string Summary, string Description)
        {
            try
            {
                string subject = ConfigurationManager.AppSettings["EmailSubject"].ToString();

                string templateFileName = ConfigurationManager.AppSettings["EmailUserTemplate"].ToString(); //format in html
                string emailBody;
                using (StreamReader sr = new StreamReader(templateFileName))
                {
                    emailBody = sr.ReadToEnd();
                }
                emailBody = emailBody.Replace("%Summary%", Summary);  
                emailBody = emailBody.Replace("%Description%", Description);

                SendUserNotificationEmail(subject, emailBody);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("EmailNotification_RunComplete() : " + ex.Message);
            }
        }

        static private void SendUserNotificationEmail(string sSubject, string sMessage)
        {
            try
            {
                string sEmailServer = ConfigurationManager.AppSettings["EmailServer"];
                string sEmailUserName = ConfigurationManager.AppSettings["EmailUserName"];
                string sEmailAddress = ConfigurationManager.AppSettings["EmailAddress"];
                string sEmailUserPwd = ConfigurationManager.AppSettings["EmailUserPwd"];
                string sEmailRecipients = ConfigurationManager.AppSettings["EmailRecipients"];

                clsEmail m_oEmail = new clsEmail();
                m_oEmail.sEmailCCAddress = ConfigurationManager.AppSettings["EmailCCRecipients"];
                m_oEmail.SendEmail(sEmailServer, sEmailUserName, sEmailUserPwd, sEmailAddress, sEmailRecipients, sSubject, sMessage, true);

            }
            catch (Exception ex)
            {
                throw new ApplicationException("SendNotificationEmail() : " + ex.Message);
            }
        }

        static private void SendErrorEmail(string sSubject, string sMessage)
        {
            try
            {
                string sEmailServer = ConfigurationManager.AppSettings["EmailServer"];
                string sEmailUserName = ConfigurationManager.AppSettings["EmailUserName"];
                string sEmailAddress = ConfigurationManager.AppSettings["EmailAddress"];
                string sEmailUserPwd = ConfigurationManager.AppSettings["EmailUserPwd"];
                string sEmailRecipients = ConfigurationManager.AppSettings["EmailErrorRecipients"];

                clsEmail m_oEmail = new clsEmail();
                m_oEmail.sEmailCCAddress = ConfigurationManager.AppSettings["EmailErrorCCRecipients"];
                m_oEmail.SendEmail(sEmailServer, sEmailUserName, sEmailUserPwd, sEmailAddress, sEmailRecipients, sSubject, sMessage, false);

            }
            catch (Exception ex)
            {
                throw new ApplicationException("SendErrorEmail() : " + ex.Message);
            }
        }

        #endregion

        static private int m_retryTimes = Convert.ToInt32(ConfigurationManager.AppSettings["SqlRetryTimes"]);
        static private TimeSpan m_retryDelay = TimeSpan.FromSeconds(Convert.ToInt32(ConfigurationManager.AppSettings["SqlRetryDelay"]));
        public static class RetryHelper
        {
            //private static ILog logger = LogManager.GetLogger(); //use a logger or trace of your choice

            public static void RetryOnException(Action operation, int retryTimes = 3, int delaySeconds = 5)
            {
                int times = m_retryTimes;
                TimeSpan delay = m_retryDelay;

                var attempts = 0;
                do
                {
                    try
                    {
                        attempts++;
                        operation();
                        break; // Sucess! Lets exit the loop!
                    }
                    catch (Exception ex)
                    {
                        if (attempts == times)
                            throw;

                        //logger.Error($"Exception caught on attempt {attempts} - will retry after delay {delay}", ex);
                        string exceptionList = "";
                        while (ex != null)
                        {
                            if (exceptionList.Length > 0)
                            {
                                exceptionList = "\r\n" + exceptionList;
                            }
                            exceptionList = ex.Message + exceptionList;
                            ex = ex.InnerException;
                        }
                        LogMsg("SQL Retrying, " + attempts + " of " + times + ", exception: " + exceptionList);

                        Task.Delay(delay).Wait();
                    }
                } while (true);
            }
        }
        
    }
}
