﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net.Mail;

namespace DDNJ_AX_Import
{
    class clsEmail
    {
        public string sEmailCCAddress
        {
            get { return m_sEmailCCAddress; }
            set { m_sEmailCCAddress = value; }
        }
        private string m_sEmailCCAddress = "";
        private System.Net.Mail.MailPriority m_Priority = MailPriority.Normal;


        public bool SendEmail(string sEmailServer,
                              string sEmailUserNameP,
                              string sEmailPassword,
                              string sFromEmailAddress,
                              string sToEmailAddress,
                              string sSubject,
                              string sBody,
                              bool BodyIsHTML,
                              string[] sAttachmentFileName = null)
        {

            try
            {
                // send an email
                MailAddress maFrom = new MailAddress(sFromEmailAddress);

                MailMessage oMsg = new MailMessage();

                oMsg.From = maFrom;

                if (sToEmailAddress.Contains(","))
                {
                    string[] sTo = sToEmailAddress.Split(',');
                    for (int i = 0; i < sTo.Length; i++)
                    {
                        oMsg.To.Add(new MailAddress(sTo[i]));
                    }
                }
                else
                {
                    if (sToEmailAddress.Contains(";"))
                    {
                        string[] sTo = sToEmailAddress.Split(';');
                        for (int i = 0; i < sTo.Length; i++)
                        {
                            oMsg.To.Add(new MailAddress(sTo[i]));
                        }
                    }
                    else
                    {
                        oMsg.CC.Add(new MailAddress(sToEmailAddress));
                    }
                }

                if (m_sEmailCCAddress.Contains(","))
                {
                    string[] sTo = sToEmailAddress.Split(',');
                    for (int i = 0; i < sTo.Length; i++)
                    {
                        oMsg.CC.Add(new MailAddress(sTo[i]));
                    }
                }
                else
                {
                    if (m_sEmailCCAddress.Contains(";"))
                    {
                        string[] sTo = sToEmailAddress.Split(';');
                        for (int i = 0; i < sTo.Length; i++)
                        {
                            oMsg.CC.Add(new MailAddress(sTo[i]));
                        }
                    }
                    else
                    {
                        if (m_sEmailCCAddress != "")
                        {
                            oMsg.CC.Add(new MailAddress(m_sEmailCCAddress));
                        }
                    }
                }

                oMsg.Subject = sSubject;

                if (BodyIsHTML) oMsg.IsBodyHtml = true;

                oMsg.Body = sBody;

                // need a username and password
                SmtpClient oMailer = new SmtpClient(sEmailServer);

                System.Net.NetworkCredential basicAuthenticationInfo = new System.Net.NetworkCredential(sEmailUserNameP, sEmailPassword);
                oMailer.UseDefaultCredentials = false;
                oMailer.Credentials = basicAuthenticationInfo;

                oMailer.EnableSsl = false;

                if (sAttachmentFileName != null)
                {
                    for (int i = 0; i < sAttachmentFileName.Length; i++)
                    {
                        if (sAttachmentFileName[i].Length > 0)
                        {
                            oMsg.Attachments.Add(new System.Net.Mail.Attachment(sAttachmentFileName[i]));
                        }
                    }
                }

                oMsg.Priority = m_Priority;

                // send the message with SmtpClient
                oMailer.Send(oMsg);

                return true;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("SendEmail() : " + ex.Message);
            }

        }
    }
}
